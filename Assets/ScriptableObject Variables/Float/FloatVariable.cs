﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject Data / Variable / Float")]
public class FloatVariable : AbstractVariable<float>
{
    
}
