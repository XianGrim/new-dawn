﻿using UnityEngine;

public class AbstractVariable<T> : ScriptableObject
{
    public T Value;
}
