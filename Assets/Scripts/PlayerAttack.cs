﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    private void Start()
    {
        
    }

    private void Update()
    {
        ProcessInputs();
    }

    private void ProcessInputs()
    {
        if (Input.GetMouseButtonDown(0))
            PrimaryAttack();

        if (Input.GetMouseButtonDown(1))
            SecondaryAttack();
    }

    private void PrimaryAttack()
    {
        // Jouer l'animation de l'attaque
        // Si ça touche un ennemi, lui enlever de la vie
    }

    private void SecondaryAttack()
    {
        // Jouer l'animation de l'attaque
        // Si ça touche un ennemi, lui enlever de la vie
    }
}
