﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateShield : Skill
{
    [SerializeField]
    private Player _player;

    public override void UseSkill()
    {
        Debug.Log("Shield before using skill: " + _player._vitality._shield.ToString());
        _player._vitality._shield = 100;
        _active = true;
        Debug.Log("Shield after using skill: " + _player._vitality._shield.ToString());
        UpdateShield();
    }

    private void UpdateShield()
    {
        if (_timer >= _coolDown/2)
        {
            _player._vitality._shield = 0;
            _active = false;
            Debug.Log("Shield when skill is finished: " + _player._vitality._shield.ToString());
        }
    }
}
