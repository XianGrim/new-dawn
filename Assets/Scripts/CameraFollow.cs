﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform _character;

    private void Update()
    {
        transform.position = new Vector3(_character.position.x, _character.position.y, -10);
    }
}
