﻿using UnityEngine;

public abstract class AbstractSkill : ScriptableObject
{
    [SerializeField]
    protected string _name;
    [SerializeField]
    protected string _description;

    public string SkillName => _name;

    public abstract void Execute();
}