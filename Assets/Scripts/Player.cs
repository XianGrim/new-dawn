﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public struct Progression
    {
        public int _level;
        public float _experience;
        public float _maxExperience;

        public Progression(int level = 1, float experience = 0, float maxExperience = 100)
        {
            _level = level;
            _experience = experience;
            _maxExperience = maxExperience;
        }
    }
    public Progression _progression;

    public struct Vitality
    {
        public int _life;
        public int _maxLife;
        public int _shield;
        public int _maxShield;

        public Vitality(int life = 100, int maxLife = 100, int shield = 0, int maxShield = 100)
        {
            _life = life;
            _maxLife = maxLife;
            _shield = shield;
            _maxShield = maxShield;
        }
    }
    public Vitality _vitality;

    public struct Stats
    {
        public int _damages;
        public int _freeStats;
        public int _strength;
        public int _agility;
        public int _intelligence;
        public int _luck;

        public Stats(int damages = 10, int freeStats = 0, int strength = 0, int agility = 0, int intelligence = 0, int luck = 0)
        {
            _damages = damages;
            _freeStats = freeStats;
            _strength = strength;
            _agility = agility;
            _intelligence = intelligence;
            _luck = luck;
        }
    }
    public Stats _stats;


    public float Experience
    {
        get { return _progression._experience; }
        set
        {
            _progression._experience += value;
            if (_progression._experience >= _progression._maxExperience)
            {
                LevelUp();
                _progression._maxExperience += 500;
                _progression._experience = 0;
            }
        }
    }

    private void Awake()
    {
        _progression = new Progression();
        _vitality = new Vitality();
        _stats = new Stats();
    }

    private void Start()
    {

    }

    private void Update()
    {
        GameOver();

        //Experience += 1;
    }

    private void LevelUp()
    {
        _progression._level++;
 
        _vitality._maxLife += 20;
        _vitality._life = _vitality._maxLife;

        _stats._freeStats += 5;

        // Add skill if I reached the right level
    }

    private void GameOver()
    {
        if (_vitality._life <= 0)
        {
            // Restart game
        }
    }
}
