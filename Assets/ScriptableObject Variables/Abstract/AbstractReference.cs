﻿[System.Serializable]
public class AbstractReference<T, U> where U : AbstractVariable<T>
{
    public bool UseConstant = true;
    public T ConstantValue = default(T);
    public U Variable = default(U);

    public T Value
    {
        get { return UseConstant ? ConstantValue : Variable.Value; }
    }
}
