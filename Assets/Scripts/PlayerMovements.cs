﻿using UnityEngine;
using System;

public class PlayerMovements : MonoBehaviour
{
    public Camera _camera;
    public Rigidbody2D _rb;

    private Vector3 _target;

    [Header("Character attributes")]
    public Vector2 _movementDirection;
    public float _baseSpeed;
    public float _speed;

    [SerializeField] int _angleOffset;

    private void Update()
    {
        var dir = Input.mousePosition - _camera.WorldToScreenPoint(transform.position);
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + _angleOffset;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        ProcessInputs();
        Move();
    }

    private void ProcessInputs()
    {
        _movementDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        _speed = Mathf.Clamp(_movementDirection.magnitude, 0.0f, 1.0f);
        _movementDirection.Normalize();
    }

    private void Move()
    {
        _rb.velocity = _movementDirection * _speed * _baseSpeed;
    }
}

/*_target = _camera.ScreenToWorldPoint(Input.mousePosition);
_target.z = 0;
transform.position = Vector3.MoveTowards(transform.position, _target, _speed * Time.deltaTime);*/
