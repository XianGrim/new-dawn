﻿using UnityEngine;

public abstract class Skill : MonoBehaviour
{
    [SerializeField]
    private string _name;
    [SerializeField]
    private string _description;
    [SerializeField]
    private int _skillLevel;
    [SerializeField]
    protected float _coolDown;

    protected float _timer; // Make a method to check whether the timer has reached its end or not, get rid of booleans
    private bool _hasReachedCooldown;
    protected bool _active;

    [SerializeField]
    private KeyCode _key;

    private void Awake()
    {
        _timer = _coolDown;
        _hasReachedCooldown = true;
        _active = false;
    }

    public void Update()
    {
        if (_timer < _coolDown)
            _timer += Time.deltaTime;
        else
        {
            if (Input.GetKeyDown(_key))
            {
                _timer = 0;
                UseSkill();
                _hasReachedCooldown = false;
            }
        }

        if (_timer >= _coolDown && !_hasReachedCooldown)
        {
            _hasReachedCooldown = true;
            OnSkillReset();
        }
    }

    public abstract void UseSkill();
    public virtual void OnSkillReset() {
        
    }
}
